<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

// dynamic compilation of the default option variable name
/** @var ithive_clear|string $modClassName */
$modClassName = str_replace('.', '_', substr(strrchr(__DIR__, DIRECTORY_SEPARATOR), 1));
if( !class_exists($modClassName) ) return;

$defaultOptionVarName = $modClassName.'_default_option';

$$defaultOptionVarName = array(
	"MODULE_PROP_CHECKBOX_A" => "N",
	"MODULE_PROP_TEXT_A" => Loc::getMessage($modClassName::MODULE_LANG_PREFIX.'PROP_TEXT_A'),
	"MODULE_PROP_TEXTAREA_A" => Loc::getMessage($modClassName::MODULE_LANG_PREFIX.'PROP_TEXTAREA_A')

);