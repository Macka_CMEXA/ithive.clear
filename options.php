<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();
/**
 * @var CMain $APPLICATION
 * @var string $REQUEST_METHOD POST|GET
 * @var string $save Var by $_REQUEST['save']
 * @var string $apply Var by $_REQUEST['apply']
 * @var string $RestoreDefaults Var by $_REQUEST['RestoreDefaults']
*/

// module object was created by /bitrix/modules/main/admin/settings.php. The class should be loaded.


/** @var string module ID. This var SHOULD BE DEFINED!!!!!! */
$module_id = substr(strrchr(__DIR__, DIRECTORY_SEPARATOR), 1);
/** @var ithive_clear|string $modClassName */
$modClassName = str_replace('.', '_', $module_id);
if( !class_exists($modClassName) ) return;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Config\Option;
//    Bitrix\Main\Loader;

// get module rights
$RIGHT = $APPLICATION->GetGroupRight($module_id);

if($RIGHT >= "R") :
	// include lang files
	Loc::loadMessages($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/settings.php"); // tabs names
	Loc::loadMessages($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php"); // button names
	Loc::loadMessages(__FILE__);

	/** @var integer Counter of Notes (for draw in next code, and step by step generate note points) */
	$iNoteCounter = 1;
	/** @var array Fields for admin function draw options __AdmSettingsDrawList($module_id, $arAllOptions);*/
	$arAllOptions = array(
		// key used as tab name
		'tab_set' => array(
			// string field always is the a section name
			Loc::getMessage($modClassName::MODULE_LANG_PREFIX."SECTION_NAME_A"),
			// note text field
			array('note' => Loc::getMessage($modClassName::MODULE_LANG_PREFIX."NOTE_A")),
			// input checkbox
			array(
				"MODULE_PROP_CHECKBOX_A", // id=
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."CHECKBOX_NAME_A"), // name=
				'', // val= ( empty ? default_option_value : this )
				array(
					"checkbox",
					'Y',
					'onclick = ""'
				),
				'', // =='Y' ? 'disabled' : ''
				$iNoteCounter++, // note tag string
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."CHECKBOX_COMMENT_A"), // none official comment string (draw past in code)
			),
			// input field
			array(
				"MODULE_PROP_TEXT_A", // id=
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."TEXT_NAME_A"), // name=
				'', // val= ( empty ? default_option_value : this )
				array(
					"text", // type
					50, // length
				),
				'', // =='Y' ? 'disabled' : ''
				$iNoteCounter++, // note tag string
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."TEXT_COMMENT_A"), // none official comment string (draw past in code)
			),
			// textarea
			array(
				"MODULE_PROP_TEXTAREA_A", // id=
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."TEXTAREA_NAME_A"), // name=
				'', // val= ( empty ? default_option_value : this )
				array(
					"textarea", // type
					5, // rows
					50 // cols
				),
				'', // =='Y' ? 'disabled' : ''
				$iNoteCounter++, // note tag string
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."TEXTAREA_COMMENT_A"), // none official comment string (draw past in code)

			),
			// static text
			array(
				'', // empty is not option
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."STATIC_TEXT_NAME_A"), // field name
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."STATIC_TEXT_VALUE_A"), // text field value
				array('statictext') // text string type
				// no more options for static text
			),
			// static html
			array(
				'', // empty is not option
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."STATIC_HTML_NAME_A"), // field name
				Loc::getMessage($modClassName::MODULE_LANG_PREFIX."STATIC_HTML_VALUE_A"), // text field value
				array('statichtml') // text string type
				// no more options for static text
			),
			// string field always is the a section name ( but empty as a large break line :) )
			'',
		),
	);

	/** @var array Collect notes which we cant to draw at admin function now, we will draw it late */
	$arNotes = array();
	foreach($arAllOptions as $arOptionTab){
		if(is_array($arOptionTab)) {
			foreach($arOptionTab as $arOption) {
				if(is_array($arOption) && isset($arOption[5]) && isset($arOption[6])){
					$arNotes[$arOption[5]] = $arOption[6];
				}
			}
		}
	}

	/** @var array Tabs params */
	$arTabs = array(
		array(
			"DIV" => "tab_set", // content div id=
			"TAB" => Loc::getMessage("MAIN_TAB_SET"), // tab name
			"TITLE" => Loc::getMessage("MAIN_TAB_TITLE_SET"), // tab content title
//			"ONSELECT" => "alert()", //  js on tab select ( ony select, not on load )
		),
		array(
			"DIV" => "tab_rights", // content div id=
			"TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"), // tab name
			"TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS") // tab content title
		),
	);

	/** @var array Available buttons array */
	$arButtons = array (
		"btnSave" => true,
		"btnApply" => true,
		"btnCancel" => true,
		"btnSaveAndAdd" => false,
	);

	// some action
    if ($REQUEST_METHOD == "POST" && strlen($save . $apply . $RestoreDefaults) > 0 && $RIGHT == "W" && check_bitrix_sessid()) {
//	    require_once(__DIR__."/prolog.php");

        if (strlen($RestoreDefaults) > 0) {
            Option::delete($module_id);
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            $resRights = \CGroup::GetList($v1="id",$v2="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
            while($arRight = $resRights->Fetch())
                $APPLICATION->DelGroupRight($module_id, array($arRight["ID"]));
        }
        else {
            foreach ($arAllOptions as $arOptionTab) {
                if (is_array($arOptionTab)) {
                    foreach ($arOptionTab as $arOption) {
                        // skip sections, static fields, separators
                        if (!is_array($arOption) || $arOption[0] == '') {
                            continue;
                        }

                        $name = $arOption[0];
                        if ($arOption[3][0] != 'checkbox') {
                            $val = $_REQUEST[$name];
                        }
                        else {
                            $val = ($_REQUEST[$name]) ? $_REQUEST[$name] : 'N';
                        }

                        // @todo: secure settings here!

                        Option::set($module_id, $name, $val);
                    }
                }
            }
        }

        // @fixme: file /bitrix/modules/main/admin/group_rights.php request $Update, not $save
		$Update = $save.$apply;
	}
	?>
	<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($module_id)?>&amp;lang=<?=LANGUAGE_ID?>">
		<?
		$tabControl = new CAdminTabControl("tabControl", $arTabs);
		$tabControl->Begin();
		$tabControl->BeginNextTab();
		__AdmSettingsDrawList($module_id, $arAllOptions['tab_set']);

		// rights tab
		$tabControl->BeginNextTab();
        /** @noinspection PhpIncludeInspection */
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");

		// buttons wrapper float div
		$tabControl->Buttons($arButtons); // variable type error by bitrix

		// add custom button
		?>
		<input
			type="submit"
			name="RestoreDefaults"
			title="<?echo Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS")?>"
			onclick="if(!confirm('<?=AddSlashes(Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>'))return false;"
			value="<?echo Loc::getMessage("MAIN_RESTORE_DEFAULTS")?>"
		/>

		<?=bitrix_sessid_post();?>
		<?$tabControl->End();?>
	</form>

	<?
	// draw notes
	if(!empty($arNotes)){
		echo BeginNote();
		foreach($arNotes as $i => $str){
			echo '<span class="required"><sup>'.$i.'</sup></span> '.$str.'<br />';
		}
		echo EndNote();
	}

// not need if we haven't right between D and R. If user have only D, this page will be never shown to him
//else :
//	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
//    CAdminMessage::ShowMessage(array(
//		"TYPE" => "ERROR",
//		"MESSAGES" => "Access denied",
//		"DETAILS" => "Access denied",
//		"HTML" => true,
//	));

endif;