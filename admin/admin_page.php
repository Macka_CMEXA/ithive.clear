<?php
/** @var $APPLICATION CMain */
/** @noinspection PhpIncludeInspection */
/* BX_ROOT is not defined now */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Config\Option,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc;

//CJSCore::Init('jquery');
//$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/ithive.mailinglist.css", true);
//$APPLICATION->AddHeadScript('/bitrix/js/'.$moduleID.'/event_creation.js');
$APPLICATION->SetTitle(Loc::getMessage("ITHIVE_CLEAR_PAGE_A_TITLE"));

/** @noinspection PhpIncludeInspection */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

Loc::loadMessages(__FILE__);

// request module prolog for admin pages ( define ADMIN_MODULE_NAME, ADMIN_MODULE_ICON )
/** @noinspection PhpIncludeInspection */
require_once(Loader::getLocal('modules/ithive.clear/prolog.php'));

// MODULE INSTALLED?
if(!Loader::includeSharewareModule($moduleID = GetModuleID(__FILE__))){
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    CAdminMessage::ShowMessage(Loc::getMessage('ITHIVE_CLEAR_MODULE_NOT_INSTALLED', array('#MODULE_ID#' => $moduleID)));
    /** @noinspection PhpIncludeInspection */
    require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
}
// ACCESS RIGHTS
$RIGHT = $APPLICATION->GetGroupRight($moduleID);
if ($RIGHT < "R"){
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    CAdminMessage::ShowMessage(Loc::getMessage("ACCESS_DENIED"));
    /** @noinspection PhpIncludeInspection */
    require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
}


?>
TEXT HERE...


<?
/** @noinspection PhpIncludeInspection */
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
?>
