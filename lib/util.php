<?php
namespace ITHive\Clear;

/**
 * Class Util
 * @package ITHive\Clear
 */
class Util {
    public static $moduleID;

    public static function getModuleID(){
        if (is_null(self::$moduleID)) {
            $arr = explode("/", __FILE__);
            $i = array_search("modules", $arr);
            self::$moduleID = $arr[$i + 1];
        }
        return self::$moduleID;
    }

}