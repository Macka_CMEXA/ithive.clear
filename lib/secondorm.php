<?php
namespace ITHive\Clear;
use Bitrix\Main\Entity;

/**
 * @package ITHive\Clear
 */
class SecondORMTable extends Entity\DataManager {

    /**
     * Set specify the table name.
     * In this example, `testtable`. If you do not define this method, the table name will be generated automatically from the namespaces and class names for the entity it will be `b_ithive_clear_testtable`.
     * I comment it by default
     *
     * @return string
     */
//	public static function getTableName() {
//		return 'testtable';
//	}

	/**
     * @see http://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=4803&LESSON_PATH=3913.5062.5748.4803
	 * @return array
	 */
	public static function getMap() {
		return array(
		    // default integer
            new Entity\IntegerField('ID',
                array(
                    'primary' => true,
                    'autocomplete' => true,
//                  'title' => 'ID',  // it set by default column name
                )
            ),

            // external field to link with second table
            new Entity\IntegerField('FIRST_ID',
                array(
                    'required' => true,
                    'title' => 'Идентификатор поля в первой таблице',
                )
            ),
            // link field
            new Entity\ReferenceField('FIRST',
                'ITHive\Clear\FirstORM',
                array('=this.FIRST_ID' => 'ref.ID')
            )
		);
	}

}
