<?
$MESS['ITHIVE_CLEAR_SECTION_NAME_A'] = 'Первый раздел настроек';

$MESS['ITHIVE_CLEAR_NOTE_A'] = 'В данном разделе представлены все возможные тестовые настройки для отрисовки функциями битрикса в административном интерфейсе';

$MESS['ITHIVE_CLEAR_CHECKBOX_NAME_A'] = 'Свойство checkbox';
$MESS['ITHIVE_CLEAR_TEXT_NAME_A'] = 'Текстовое поле';
$MESS['ITHIVE_CLEAR_TEXTAREA_NAME_A'] = 'Текстовый блок';
$MESS['ITHIVE_CLEAR_STATIC_TEXT_NAME_A'] = 'Обычный текст';
$MESS['ITHIVE_CLEAR_STATIC_HTML_NAME_A'] = 'Обычный html код';


$MESS['ITHIVE_CLEAR_STATIC_TEXT_VALUE_A'] = 'Text text without html code';
$MESS['ITHIVE_CLEAR_STATIC_HTML_VALUE_A'] = '<span style="color: orange;">Static html code</span>';



$MESS['ITHIVE_CLEAR_CHECKBOX_COMMENT_A'] = 'Комментарий к свойству checkbox';
$MESS['ITHIVE_CLEAR_TEXT_COMMENT_A'] = 'Комментарий к текстовому полю';
$MESS['ITHIVE_CLEAR_TEXTAREA_COMMENT_A'] = 'Комментарий к текстовому блоку';

