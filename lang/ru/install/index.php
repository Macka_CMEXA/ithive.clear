<?
$MESS['ITHIVE_CLEAR_PARTNER_NAME'] = '<img src="/local/modules/ithive.clear/partner_icon.png" style="padding-right: 10px; vertical-align:
bottom;"><b>IT-Улей</b>';
$MESS['ITHIVE_CLEAR_PARTNER_URI'] = 'http://www.it-hive.ru/';
$MESS['ITHIVE_CLEAR_MODULE_NAME'] = "Чистый модуль";
$MESS['ITHIVE_CLEAR_MODULE_DESC'] = "Чистый модуль для разработки.";
$MESS['ITHIVE_CLEAR_MODULE_INSTALL_TITLE'] = "Установка модуля: Чистый модуль";
$MESS['ITHIVE_CLEAR_MODULE_INSTALL_TITLE_STEP1'] = "Установка модуля: Чистый модуль. Шаг 1.";
$MESS['ITHIVE_CLEAR_MODULE_INSTALL_TITLE_STEP2'] = "Установка модуля: Чистый модуль. Шаг 2.";
$MESS['ITHIVE_CLEAR_MODULE_UNINSTALL_TITLE'] = "Деинсталляция модуля: Чистый модуль";

$MESS['MOD_ERROR_NOT_CORRECT_VERSION'] = 'Информация о версии заполнена некорректно в файле #FILE_PATH# ';
