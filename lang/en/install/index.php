<?
$MESS['ITHIVE_CLEAR_PARTNER_NAME'] = '<img src="/local/modules/ithive.clear/partner_icon.png" style="padding-right: 10px; vertical-align:
bottom;"><b>IT-Hive</b>';
$MESS['ITHIVE_CLEAR_PARTNER_URI'] = 'http://www.it-hive.ru/';
$MESS['ITHIVE_CLEAR_MODULE_NAME'] = "Clear module";
$MESS['ITHIVE_CLEAR_MODULE_DESC'] = "Clear module for develope.";
$MESS['ITHIVE_CLEAR_MODULE_INSTALL_TITLE'] = "Module install: Clear module";
$MESS['ITHIVE_CLEAR_MODULE_INSTALL_TITLE_STEP1'] = "Module install: Clear module. Step 1.";
$MESS['ITHIVE_CLEAR_MODULE_INSTALL_TITLE_STEP2'] = "Module install: Clear module. Step 2.";
$MESS['ITHIVE_CLEAR_MODULE_UNINSTALL_TITLE'] = "Module uninstall: Clear module";

$MESS['MOD_ERROR_NOT_CORRECT_VERSION'] = 'Incorrect version info at #FILE_PATH# ';