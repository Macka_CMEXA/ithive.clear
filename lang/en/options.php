<?
$MESS['ITHIVE_CLEAR_SECTION_NAME_A'] = 'First settings section';

$MESS['ITHIVE_CLEAR_NOTE_A'] = 'This section lists all test setup for rendering functions Bitrix administrative interface';

$MESS['ITHIVE_CLEAR_CHECKBOX_NAME_A'] = 'Property checkbox';
$MESS['ITHIVE_CLEAR_TEXT_NAME_A'] = 'Property text';
$MESS['ITHIVE_CLEAR_TEXTAREA_NAME_A'] = 'Text block';
$MESS['ITHIVE_CLEAR_STATIC_TEXT_NAME_A'] = 'Simply text';
$MESS['ITHIVE_CLEAR_STATIC_HTML_NAME_A'] = 'Simply html code';


$MESS['ITHIVE_CLEAR_STATIC_TEXT_VALUE_A'] = 'Text text without html code';
$MESS['ITHIVE_CLEAR_STATIC_HTML_VALUE_A'] = '<span style="color: orange;">Static html code</span>';



$MESS['ITHIVE_CLEAR_CHECKBOX_COMMENT_A'] = 'Note for property checkbox';
$MESS['ITHIVE_CLEAR_TEXT_COMMENT_A'] = 'Note for text property';
$MESS['ITHIVE_CLEAR_TEXTAREA_COMMENT_A'] = 'Note for text block';

