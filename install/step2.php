<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();
/* there is no need, this was checked by Bitrix */
//if (!check_bitrix_sessid()) return;

/**
 * @var CMain $APPLICATION
 * @var ithive_clear $Module Setup by bitrix modules installer
 */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\NotSupportedException;

Loc::loadMessages(__FILE__);

// check the availability of Bitrix created variable $Module for working with module
if (!isset($Module->MODULE_ID)) {
    throw new NotSupportedException(Loc::getMessage("MOD_NOT_SUPPORTED"));
}

// id = <MODULE_ID>
// lang = <LANGUAGE_ID>
// install = <anything>
// step = <step_number> - go to <step_number> REQUIRED! last step might have nonexistent step number
?>
<form action="<?= $APPLICATION->GetCurPage() ?>" name="<?= $Module->MODULE_ID?>">
    <input type="hidden" name="id" value="<?= $Module->MODULE_ID ?>">
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
    <input type="hidden" name="install" value="Y">
    <input type="hidden" name="step" value="3">
    <?= bitrix_sessid_post() ?>

    <span>Step 2 test</span>
    <ul>
        <li>
            test
        </li>
        <li>
            test
        </li>
        <li>
            test
        </li>
        <li>
            test
        </li>
        <li>
            test
        </li>
        <li>
            test
        </li>
    </ul>

    <input type="submit" name="" value="<? echo Loc::getMessage($Module::MODULE_LANG_PREFIX . "MODULE_INSTALL_FINISH") ?>">
<form>