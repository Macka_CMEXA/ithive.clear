<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();
/**
 * @var CMain $APPLICATION
*/

use Bitrix\Main\Localization\Loc,
//  Bitrix\Main\SystemException,
    Bitrix\Main\IO\FileNotFoundException;

/* next only meant to search here, or we probably not found it, first message include this lang file, and store it at Loc::$includedFiles */
Loc::loadMessages(__FILE__);

if(class_exists("ithive_clear")) return;
/**
 * Main class for module ITHive.
 * @link http://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=2824&LESSON_PATH=3913.4609.2824
 */
Class ithive_clear extends CModule
{
	const
		MODULE_ID = "ithive.clear",
		MODULE_LANG_PREFIX = 'ITHIVE_CLEAR_';

	public
		$MODULE_ID = "ithive.clear",
		$MODULE_SORT = -1, // that's means first point after main
		$MODULE_VERSION,
		$MODULE_VERSION_DATE,
		$MODULE_NAME,
		$MODULE_DESCRIPTION,

		$PARTNER_NAME,
		$PARTNER_URI;

	public function __construct() {

        try {
			$this->setVersion();
		}
		catch(Exception $ex) { // all type exceptions
			// @TODO log to error/warning log this behavior
			// set minimum for update at all
			$this->MODULE_VERSION = '0.0.1';
			$this->MODULE_VERSION_DATE = "1970-01-01 00:00:00";
		}

		$this->MODULE_NAME = Loc::getMessage(self::MODULE_LANG_PREFIX . "MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage(self::MODULE_LANG_PREFIX . "MODULE_DESC");

		$this->PARTNER_NAME = Loc::getMessage(self::MODULE_LANG_PREFIX . "PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage(self::MODULE_LANG_PREFIX . "PARTNER_URI");
	}

	/**
	 * Setup module version
	 *
	 * @throws Exception
	 * @throws FileNotFoundException
	 */
	private function setVersion(){
		if(!file_exists(__DIR__ . "/version.php")){
			throw new FileNotFoundException(__DIR__ . "/version.php");
		}
		/** @var array Should be reassigned by version.php */
		$arModuleVersion = array();
		include(__DIR__ . "/version.php");

		if (!isset($arModuleVersion["VERSION"]) || !isset($arModuleVersion["VERSION_DATE"]) ) {
			throw new Exception(Loc::getMessage('MOD_ERROR_NOT_CORRECT_VERSION', array('#FILE_PATH' =>  __DIR__ . '/version.php')), 1);
		}

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
	}

	public function DoInstall() {
		global $APPLICATION;
		$step = IntVal($_REQUEST["step"]);


		switch ($step) {
			case '0' : // init point, we start install here ( step var not initialized now, not break after this )

            //  If you want init without steps, than comment all next case for choose by default
			case '1' : // go to step 1
				// include step file
				$APPLICATION->IncludeAdminFile(
					GetMessage(self::MODULE_LANG_PREFIX . "MODULE_INSTALL_TITLE_STEP1"),
					__DIR__ . "/step1.php"
				);
				break;

			case '2' : // go to step 2
				// include step file
				$APPLICATION->IncludeAdminFile(
					GetMessage(self::MODULE_LANG_PREFIX . "MODULE_INSTALL_TITLE_STEP2"),
					__DIR__ . "/step2.php"
				);
				break;

			default : // target not available step ( install finish actions )
				if (!$this->InstallDB() || !$this->InstallEvents() || !$this->InstallFiles()) {
					return false; // break install
				}
		}

		// Core actions. If we still here, thats all ok, lets finish it
		RegisterModule(self::MODULE_ID); // register module in system
 		return true;
	}
	public function DoUninstall() {
		global $APPLICATION;

		$step = IntVal($_REQUEST["step"]);
		switch ($step) {
			case '0' :  // init point, we start uninstall here ( step var not initialized now, not break after this )
			case '1' : // go to step 1
				$APPLICATION->IncludeAdminFile(
					GetMessage(self::MODULE_LANG_PREFIX . "MODULE_UNINSTALL_TITLE"),
					__DIR__ . "/unstep.php"
				);
				break;
//			case '2' : // go to step 2
//				// second step
//				$APPLICATION->IncludeAdminFile(
//					GetMessage(self::MODULE_ID . "MODULE_UNINSTALL_TITLE_STEP2"),
//					__DIR__ . "/step2.php"
//				);
//				break;

			default : // target not available step ( uninstall finish actions )
				// action as success all methods, else false
				if (!$this->UnInstallDB(array("savedata" => 'Y' == $_REQUEST["savedata"]))
					|| !$this->UnInstallEvents()
					|| !$this->UnInstallFiles()
				){
					return false; // break uninstall
				}
		}

		// Core actions. If we still here, thats all ok, lets finish it
		COption::RemoveOption(self::MODULE_ID); // delete all options from DB
		UnRegisterModule(self::MODULE_ID); // delete module register

		return true;
	}


	public function InstallDB() {
		global $DB;
		// careful we must have instruction CREATE TABLE IF NOT EXISTS for tables in sql file
		// or this rule for each table
		// if( !$DB->Query("SELECT 'x' FROM a_test WHERE 1=0", true) )
		$DB->RunSQLBatch(__DIR__."/db/".strtolower($DB->type)."/install.sql");

		return true;
	}
	public function UnInstallDB($arParams = array()) {
		global $DB;
		if(!array_key_exists("savedata", $arParams) || !$arParams["savedata"]) { // why double check?
			$DB->RunSQLBatch(__DIR__."/db/".strtolower($DB->type)."/uninstall.sql");
		}

		return true;
	}

	public function InstallEvents()
	{
		RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, '\ITHive\Clear\Handler', 'OnBuildGlobalMenu');

		return true;
	}
	public function UnInstallEvents()
	{
		UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, '\ITHive\Clear\Handler', 'OnBuildGlobalMenu');
		return true;
	}

	public function InstallFiles()
	{
		// copy ADMIN section files
		CopyDirFiles(
			__DIR__ . "/admin",
			$_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin",
			true,
			true
		);

		// copy Images
//		CopyDirFiles(
//			$_SERVER["DOCUMENT_ROOT"] . self::STORAGE_PATH . self::MODULE_ID . "/images",
//			$_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/".self::MODULE_ID,
//			true,
//			true
//		);
		// copy ALL namespaces components
		/*CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"] . self::STORAGE_PATH . self::MODULE_ID . "/install/components",
			$_SERVER["DOCUMENT_ROOT"] . "/bitrix/components",
			true,
			true
		);*/
		// copy ALL namespaces component template to .default template
		/*CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"] . self::STORAGE_PATH . self::MODULE_ID . "/install/templates/.default/components",
			$_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components",
			true, // rewrite
			true // recursive
		);*/

		return true;
	}
	public function UnInstallFiles()
	{
		// delete dir files (not recursive)
		DeleteDirFiles(__DIR__ ."/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
//		DeleteDirFilesEx("/bitrix/images/".self::MODULE_ID."/"); //images
//		DeleteDirFilesEx("/bitrix/js/".self::MODULE_ID."/"); //javascript

		// delete dir files (not recursive)
//		DeleteDirFiles(
//			$_SERVER["DOCUMENT_ROOT"].self::STORAGE_PATH.self::MODULE_ID."/install/components/bitrix/_test",
//			$_SERVER["DOCUMENT_ROOT"]."/bitrix/components/bitrix/_test"
//		);
		// delete test component, rm dir and all in
//		DeleteDirFilesEx("/bitrix/components/bitrix/_test"); // local path !! not absolute !!
//		DeleteDirFilesEx("/bitrix/components/bitrix/_test1"); // local path !! not absolute !!
//		DeleteDirFilesEx("/bitrix/components/bitrix/_test2"); // local path !! not absolute !!
		return true;
	}
}