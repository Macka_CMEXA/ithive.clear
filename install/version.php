<?
/**
 * Module version array, must contain VERSION key with the value as a string and VERSION_DATE key  with the value as a string formatted as 'YYYY-MM-DD H:m:s'
 * @var array
 */
$arModuleVersion = array(
    "VERSION" => "0.5.0",
    "VERSION_DATE" => "2015-03-11 19:16:00"
);

