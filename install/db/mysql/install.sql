-- Caution! Order of actions is important!
-- We need to create a dependent table first, only then the other table with binding fields

CREATE TABLE IF NOT EXISTS `b_ithive_clear_firstorm`
(
	ID int PRIMARY KEY AUTO_INCREMENT NOT NULL,
	TYPE enum('ELEMENT','SECTION', 'SECTION_PATH'),
	STRING varchar(255) DEFAULT NULL,
	DATE TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS `b_ithive_clear_secondorm` (
	ID int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
	FIRST_ID int(11),
	FOREIGN KEY (FIRST_ID) REFERENCES `b_ithive_clear_firstorm` (`ID`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);