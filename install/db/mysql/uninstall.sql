-- Caution! Order of actions is important!
-- We need to delete first the binding table, only then the source tables

DROP TABLE `b_ithive_clear_secondorm`;
DROP TABLE `b_ithive_clear_firstorm`;