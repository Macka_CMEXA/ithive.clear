<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== TRUE) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

define("ADMIN_MODULE_NAME", "ithive.clear");
define("ADMIN_MODULE_ICON",
	'<a href="stat_list.php?lang='.LANGUAGE_ID.'">'.PHP_EOL
	.'<img src="/bitrix/images/statistic/statistic.gif" width="48" height="48" border="0" alt="PROLOG_TEST_ALT" title="'."PROLOG_TEST_TITLE".'">'.PHP_EOL
	.'</a>'
);